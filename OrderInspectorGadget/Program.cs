﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Linq;
using System.Xml.Linq;
using System.IO;
using System.Text;
using Microsoft.CommerceServer.Runtime;
using Microsoft.CommerceServer.Runtime.Orders;
using Microsoft.CommerceServer.Runtime.Profiles;
using RTDbg = Microsoft.CommerceServer.Runtime.Diagnostics;
using System.Security.Cryptography;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Sockets;

namespace OrderInspectorGadget
{
    class Program
    {
        private static OrderContext _context;
        private static bool _traceToLog;
        private static string _traceLogPath;
      
        private static string _errorLogPath;
        private static StreamWriter _traceLogWriter;
        private static StreamWriter _errorLogWriter;
        private static IPEndPoint _ipEndPoint;
        private static string _graphiteAPIKey;
        private static string _magentoOrdersPath;
        private static string _uniqueFileId;
        private static DateTime _aMonthAgo;
        private static string _deployVersion;
        private static int _totalOrders;
        private static DateTime _now;

        static void Main(string[] args)
        {
            try
            {
                _now = DateTime.Now;
                _aMonthAgo = (new DateTime(_now.Year, _now.Month, _now.Day)).AddMonths(-1);
                // For testing in sandbox _aMonthAgo = new DateTime(2014, 06, 18);
                _uniqueFileId = _now.ToString("yyyy_MM_dd_HH_mm_ss");
                _deployVersion = ConfigurationManager.AppSettings["DeployVersion"];
                _traceToLog = ConfigurationManager.AppSettings["TraceToLog"] == "true" ? true : false;
                _traceLogPath = _traceToLog ? ConfigurationManager.AppSettings["TraceLogPath"] : "trace.log";
                _traceLogPath = Path.IsPathRooted(_traceLogPath) ? _traceLogPath : Path.Combine(Environment.CurrentDirectory, _traceLogPath);
                //_traceLogPath = _traceLogPath + "\\" + "trace_" + _uniqueFileId + "_" + ConfigurationManager.AppSettings["LogFilename"];
               
                _errorLogPath = ConfigurationManager.AppSettings["ErrorLogPath"];
                _errorLogPath = Path.IsPathRooted(_errorLogPath) ? _errorLogPath : Path.Combine(Environment.CurrentDirectory, _errorLogPath);
                //errorLogPath = _errorLogPath + "\\" + "error_" + _uniqueFileId + "_" + ConfigurationManager.AppSettings["ErrorFilename"];
             
                _magentoOrdersPath = ConfigurationManager.AppSettings["MagentoOrdersPath"];
                _magentoOrdersPath = !String.IsNullOrEmpty(_magentoOrdersPath) && Path.IsPathRooted(_magentoOrdersPath) ? _magentoOrdersPath : Path.Combine(Environment.CurrentDirectory, _magentoOrdersPath);
                _magentoOrdersPath = Path.Combine(_magentoOrdersPath, _now.ToString("yyyy-MM-dd"));

                if (!Directory.Exists(_magentoOrdersPath))
                    Directory.CreateDirectory(_magentoOrdersPath);
                if (!Directory.Exists(_traceLogPath))
                    Directory.CreateDirectory(_traceLogPath);
                if (!Directory.Exists(_errorLogPath))
                    Directory.CreateDirectory(_errorLogPath);

                _traceLogWriter = _traceToLog && !String.IsNullOrEmpty(_traceLogPath) ? new StreamWriter( _traceLogPath + "\\" + "trace_" + _uniqueFileId + "_" + ConfigurationManager.AppSettings["LogFilename"], true) : null;
                _errorLogWriter = new StreamWriter(_errorLogPath + "\\" + "error_" + _uniqueFileId + "_" + ConfigurationManager.AppSettings["ErrorFilename"], true);


                _graphiteAPIKey = ConfigurationManager.AppSettings["GraphiteAPIKey"];

                string graphiteEndPoint = ConfigurationManager.AppSettings["GraphiteEndPoint"];
                _ipEndPoint = !String.IsNullOrEmpty(graphiteEndPoint) ? new IPEndPoint(Dns.GetHostAddresses(graphiteEndPoint)[0], 8125) : null;
            }
            catch (Exception exception)
            {
                Trace("CONFIG ERROR (see log): " + exception.Message);
                LogExceptionToFile(exception, "CONFIG ERROR");
                CloseTraceLog();
                return;
            }

            try
            {
                Trace("Initailizing Commerce Server");
                _context = OrderContext.Create(ConfigurationManager.AppSettings["Transactions"], ConfigurationManager.AppSettings["TransactionsConfig"]);

                if (args.Length >= 2 &&args[0] == "magento-mass-export-user")
                {
                     
                    //args 0: type of export, 1: store, 2 (optional): Cutoff Date
                    string store = args[1];//default 2 days ago
                    DateTime cutoffUTC = new DateTime(_now.Year, _now.Month, _now.Day, 23, 0, 0).AddDays(-8);
                    if (args.Length == 3)
                    {
                        try
                        {
                            cutoffUTC = DateTime.Parse(args[2]);
                        }
                        catch (Exception e)
                        {
                            LogExceptionToFile(e, "N/A");
                            return;
                        }
                    }
                    // For test in sandbox DateTime cutoffUTC = new DateTime(2014, 06, 15, 23, 00, 00, DateTimeKind.Utc);
                    int pageSize = int.Parse(ConfigurationManager.AppSettings["PageSize"]);
                    Trace("Order Mass User History Export Engaged!");

                    
                    ExportAllUserMagentoOrderHistory(store, cutoffUTC, pageSize);
                    
                }
                else if (args.Length == 3 && args[0] == "magento-mass-export-pagesize")
                {
                    string store = args[1];
                    int pageSize = int.Parse(args[2]);
                    Trace("Order Mass PageSize User History Export Engaged!");
                    ExportPagedMagentoOrderHistory(store, 1000, new DateTime(2014, 06, 05));
                }
                else if (args.Length == 2 && args[0] == "cc-export")
                {
                    //Guid userId = new Guid(args[1]);
                    Guid ccId = new Guid("{109fd951-58ae-4d28-865f-375e5a01743d}");
                    string cc = ExportProfileCreditCard(ccId);
                    Console.WriteLine("Your CC is: " + cc);
                }
                else if (args.Length == 2 && args[0] == "magento-history")
                {
                    //Comment out since method called is being majorly changed
                    /*
                    Trace("Order User History Mapper Engaged!");
                    Guid userId = new Guid(args[1]);
                    //Guid userId = new Guid("{2ae29f52-cd8b-49ce-a8c6-f5843173865c}");
                    DateTime cutoffDate = new DateTime(1980, 1, 1, 0, 0, 0);
                    MapUserOrderHistoryToMagentoXML(userId, String.Empty, cutoffDate); //TODO - populate real email (lookup via DB)
                     * */
                }
                else if (args.Length == 3 && args[0] == "magento-autoship")
                {
                    Guid userId = new Guid(args[1]);
                    Guid orderId = new Guid(args[2]);
                    MapAutoShipToMagentoXML(userId, orderId);
                }
                else if (args.Length == 2 && args[0] == "netsuite-order-mapper")
                {
                    Trace("NetSuite Sales Order Mapper Engaged!");
                    //Guid orderId = new Guid(args[1]);
                    Guid orderId = new Guid("CAE5EE1F-022C-476D-A371-76EF68B000C7");
                    NetSuiteService.SalesOrder salesOrder = MapCSXMLToNetSuiteSOAP(orderId);
                    Trace("DONE!");
                }
                else
                {
                    ExtractMarshalledData();
                }
            }
            catch (Exception exception)
            {
                LogExceptionToFile(exception, "UNHANDLED EXCEPTION");
                throw exception;  //TODO: change this... ugh
            }
            finally
            {
                CloseTraceLog();
            }
        }

        private static NetSuiteService.SalesOrder MapCSXMLToNetSuiteSOAP(Guid orderId)
        {
            DataSet ds = new DataSet();
            string sql = "exec GetNetSuiteOrderMappingData '" + orderId + "'";

            Trace("Fetching orders to process");

            using (var conn = new SqlConnection(ConfigurationManager.AppSettings["Transactions"]))
            {
                using (var da = new SqlDataAdapter(sql, conn))
                {
                    conn.Open();

                    da.Fill(ds);
                }
            }

            Trace(ds.Tables[0].Rows.Count.ToString() + " order records found!\n");

            DataRow dr = ds.Tables[0].Rows[0];

            string netsuiteCustomerId = dr["customer_id"].ToString();
            string cyberRequestId = dr["RequestId"].ToString();

            Trace("Processing order");

            NetSuiteService.SalesOrder salesOrder = new NetSuiteService.SalesOrder();

            XDocument doc = XDocument.Parse(dr["MarshalledData"].ToString());
            XElement po = doc.Element("PurchaseOrder");
            XElement of = po.Element("OrderForms").Element("OrderForm");
            XElement cc = of.Element("Payments").Element("CreditCardPayment");
            XElement li = of.Element("LineItems");
            XElement ba = po.Element("Addresses").Elements("OrderAddress").First(a => a.Attribute("OrderAddressId").Value == cc.Attribute("BillingAddressId").Value);
            XElement sa = po.Element("Addresses").Elements("OrderAddress").First(a => a.Attribute("OrderAddressId").Value == of.Element("Shipments").Element("Shipment").Attribute("ShippingAddressId").Value);

            salesOrder.entity = new NetSuiteService.RecordRef() { internalId = "" }; //TODO: Map Customer ID
            salesOrder.externalId = orderId.ToString();
            salesOrder.tranDate = DateTime.Parse(po.Attribute("Created").Value);
            salesOrder.orderStatus = NetSuiteService.SalesOrderOrderStatus._pendingFulfillment;
            salesOrder.isTaxable = true;

            List<NetSuiteService.CustomFieldRef> orderCustomFieldList = new List<NetSuiteService.CustomFieldRef>();

            orderCustomFieldList.Add(new NetSuiteService.SelectCustomFieldRef() { internalId = "custbody_sales_source", value = new NetSuiteService.ListOrRecordRef() { internalId = "7" } });
            orderCustomFieldList.Add(new NetSuiteService.StringCustomFieldRef() { internalId = "custbody_web_so_num", value = po.Attribute("TrackingNumber").Value });

            salesOrder.customFieldList = orderCustomFieldList.ToArray();

            NetSuiteService.BillAddress billAddress = new NetSuiteService.BillAddress();

            billAddress.billAddressee = ba.Attribute("FirstName").Value + " " + ba.Attribute("LastName").Value;
            billAddress.billAddr1 = ba.Attribute("Line1").Value;
            billAddress.billCity = ba.Attribute("City").Value;
            billAddress.billState = ba.Attribute("State").Value;
            billAddress.billZip = ba.Attribute("PostalCode").Value;
            billAddress.billPhone = ba.Attribute("DaytimePhoneNumber").Value;

            salesOrder.transactionBillAddress = billAddress;

            NetSuiteService.ShipAddress shipAddress = new NetSuiteService.ShipAddress();

            shipAddress.shipAddressee = ba.Attribute("FirstName").Value + " " + ba.Attribute("LastName").Value;
            shipAddress.shipAddr1 = ba.Attribute("Line1").Value;
            shipAddress.shipCity = ba.Attribute("City").Value;
            shipAddress.shipState = ba.Attribute("State").Value;
            shipAddress.shipZip = ba.Attribute("PostalCode").Value;
            shipAddress.shipPhone = ba.Attribute("DaytimePhoneNumber").Value;

            salesOrder.transactionShipAddress = shipAddress;

            salesOrder.subsidiary = new NetSuiteService.RecordRef() { internalId = "1" };

            salesOrder.pnRefNum = cyberRequestId;

            string processorId = String.Empty;
            string storeName = doc.Descendants("WeaklyTypedProperty").First(p => p.Attribute("Name").Value == "SiteId").Attribute("Value").Value.Substring(0, 3);

            switch (storeName)
            {
                case "360":
                    processorId = "1"; //TODO: real values??
                    break;
                case "NPP":
                    processorId = "3";
                    break;
                default: //PFD
                    processorId = "2";
                    break;
            }

            salesOrder.creditCardProcessor = new NetSuiteService.RecordRef() { internalId = processorId };
            salesOrder.ccApproved = true;

            string shippingMethodName = of.Element("Shipments").Element("Shipment").Attribute("ShippingMethodName").Value;
            string shipMethodId = String.Empty;

            switch (shippingMethodName)
            {
                case "UPS Next Day Air":
                    shipMethodId = "4"; //TODO: real values AND deal with FREE SHIPPING???
                    break;
                case "UPS 2nd Day Air":
                    shipMethodId = "4";
                    break;
                default:
                    shipMethodId = "4";
                    break;
            }

            salesOrder.shipMethod = new NetSuiteService.RecordRef() { internalId = shipMethodId };

            double shippingCost = double.Parse(po.Attribute("ShippingTotal").Value);
            salesOrder.shippingCost = shippingCost;

            List<NetSuiteService.SalesOrderItem> soItems = new List<NetSuiteService.SalesOrderItem>();

            foreach (XElement line in li.Elements("LineItem"))
            {
                NetSuiteService.SalesOrderItem soItem = new NetSuiteService.SalesOrderItem();
                soItem.job = new NetSuiteService.RecordRef() { externalId = salesOrder.externalId };
                soItem.item = new NetSuiteService.RecordRef() { internalId = GetNetSuiteItemIdFromCSVariantId(line.Attribute("ProductVariantId").Value.Replace("(BaseCatalog)", "")) };
                soItem.quantity = double.Parse(line.Attribute("Quantity").Value);
                soItem.rate = line.Attribute("PlacedPrice").Value;

                List<NetSuiteService.CustomFieldRef> itemCustomFieldList = new List<NetSuiteService.CustomFieldRef>();

                var petIdNode = doc.Descendants("WeaklyTypedProperty").FirstOrDefault(l => l.Attribute("Name").Value == "petId" && l.Attribute("Value").Value.Length > 0);
                var vetIdNode = doc.Descendants("WeaklyTypedProperty").FirstOrDefault(l => l.Attribute("Name").Value == "vetId" && l.Attribute("Value").Value.Length > 0);

                if (petIdNode != null)
                {
                    itemCustomFieldList.Add(new NetSuiteService.SelectCustomFieldRef() { internalId = "custcol_vet", value = new NetSuiteService.ListOrRecordRef() { internalId = petIdNode.Attribute("Value").Value } });
                }

                if (vetIdNode != null)
                {
                    itemCustomFieldList.Add(new NetSuiteService.SelectCustomFieldRef() { internalId = "custcol_vet", value = new NetSuiteService.ListOrRecordRef() { internalId = petIdNode.Attribute("Value").Value } });
                }

                soItem.customFieldList = itemCustomFieldList.ToArray();

                //TODO: Map HIN!?

                soItems.Add(soItem);
            }

            NetSuiteService.SalesOrderItemList orderItemList = new NetSuiteService.SalesOrderItemList();

            orderItemList.replaceAll = false;
            orderItemList.item = soItems.ToArray();

            salesOrder.itemList = orderItemList;

            return salesOrder;
        }

        private static string GetNetSuiteItemIdFromCSVariantId(string variantId)
        {
            DataSet ds = new DataSet();
            string sql = "select ITEM_ID from [Netsuite].[dbo].[NetSuiteCache_ITEMS] where ITEM_EXTID = '" + variantId + "'";

            Trace("Fetching NetSuite item: " + variantId);

            string itemId = String.Empty;

            using (var conn = new SqlConnection(ConfigurationManager.AppSettings["Transactions"]))
            {
                using (var cmd = new SqlCommand(sql, conn))
                {
                    conn.Open();

                    itemId = cmd.ExecuteScalar().ToString();
                }
            }

            return itemId;
        }

        private static string ExportProfileCreditCard(Guid creditCardId)
        {
            string connString = "Provider=CSOLEDB;Data Source=PFDSANDBOXSQL01;Initial Catalog=cSharpSite_profiles;Integrated Security=SSPI;";

            // To decrypt, specify PrivateKey1= and KeyIndex1= if you encrypted 
            // using /i 1 with ProfileKeyManager, or PrivateKey2= and KeyIndex2= if you specified 
            // /i 2 with ProfileKeyManager. 
            connString += "PublicKey=0602000000A4000052534131000400000100010067FC5C7A1D55BF0F00AEBEDC40A5E841174C9C7DB8A212486E91B24EE737B4AA1D77DEC007493D77D8B65230A09AC5213A40372CA52829EF08FE99BCCF1B67FE6608075824B442D86E76EB2F0BCF59263306B1261EE4FA5A58AB72E13EBDD0F4F398D01C61CFAF7365237878EE7BA31D3D3A0A830F9048681B738FBD929EEF9B;"
                + "PrivateKey1=0702000000A4000052534132000400000100010067FC5C7A1D55BF0F00AEBEDC40A5E841174C9C7DB8A212486E91B24EE737B4AA1D77DEC007493D77D8B65230A09AC5213A40372CA52829EF08FE99BCCF1B67FE6608075824B442D86E76EB2F0BCF59263306B1261EE4FA5A58AB72E13EBDD0F4F398D01C61CFAF7365237878EE7BA31D3D3A0A830F9048681B738FBD929EEF9BDDF011D0F2A6CE79941F44DE15147E6CCB9C2F63DD158DB7E38A052DA449DE3E03CDF3B57C7841EDEF9BECC38917F404BA083EDC2182D6FDA3F3DAD14B69C8CC13CCF813A8ACE29773DF9311F61098098F9008752BFA78DCB8DD7FC9D93C7DA1F2699CAB8CB3C74416E245F511415CD7039CD6B5CF15F6263B6FBE129CB3EFC22575CC444CFAD3CF9EDA57B7EA848DED8D96B0EA822BC07111EFCE7B6E1D3A7DA9D953081419EF636ED4679434017C20C488B6D71954BB7E3367017898936428A553A0222814B1234517B61A49103FA3B5FD6CE0EFD49323547CD9A9F744859546BE9259B6EA40502548B685EA7324FCAFABD7C40AD78901BF3CD8A26264EA484E834139ACB32947DE742C36AF4A83BA9FBC47F718D55F2E36C7BC930D93F3639CB95B54DC883600B06E9C347E5F1DC3E27C59B273D804948AB9E9C23D752684D13F68CEDA6B3FC83E43943ED5A74DC3DBC0D526D4A973953399AA183B222C035F7AB933A07FC2975A59F0AB7527302055AD2EA8708B5E3A7BA0C4D8891E32E95F14DCC0319ECBB039037E2E25A201ED13A9E60ECC3C0C4EBA826B2BB50989C518458761EAB73A1BBC6716E6F49DCD14DBFB21E67AFFECA851807C5BC4A1C569;"
                + "KeyIndex=1";

            RTDbg.ConsoleDebugContext debugContext = new RTDbg.ConsoleDebugContext(RTDbg.DebugMode.Debug);
            Console.Write("Initializing Profile...");
            ProfileContext profileContext = new ProfileContext(connString, debugContext);
            Console.Write("DONE!\n");
            Profile profile = profileContext.GetProfile(creditCardId.ToString("B"), "CreditCard");
            //Profile profile = profileContext.GetProfile("{2ae29f52-cd8b-49ce-a8c6-f5843173865c}", "UserObject");

            //return profile["GeneralInfo.cc_number"].Value.ToString();
            foreach (ProfilePropertyGroup ppg in profile.PropertyGroups)
            {
                Trace("Property Group: " + ppg.Name);

                foreach (ProfileProperty pp in ppg.Properties)
                {
                    Trace("\t\t " + pp.Name);//+ " = \"" + pp.Value.ToString() + "\"");
                    if (pp.Name == "cc_number" && pp.Value != null)
                    {
                        Trace("Value: " + pp.Value.ToString());
                    }
                }
            }

            return String.Empty;
        }

        private static void CloseTraceLog()
        {
            if (_traceLogWriter != null)
            {
                _traceLogWriter.Flush();
                _traceLogWriter.Close();
                _traceLogWriter.Dispose();
            }
        }

        private static void ExportPagedMagentoOrderHistory(string storeCode, int pageSize, DateTime cutoff)
        {
            /*
            storeCode = string.IsNullOrEmpty(storeCode) ? "360" : storeCode;
            
            if (cutoff == null)
            {
                DateTime now = DateTime.Now.AddDays(-7);
                
                cutoff = new DateTime(now.Year, now.Hour, now.Minute);
            }
            DataSet ds = new DataSet();
            Trace("Fetching orders to export");

            using (var conn = new SqlConnection(ConfigurationManager.AppSettings["Transactions"]))
            {
                var command = new SqlCommand();
                command.CommandText = "dbo.getUserMaxOrderDateInfo";
                command.CommandType = System.Data.CommandType.StoredProcedure;
                command.Connection = conn;

                command.Parameters.AddWithValue("CutoffDate", cutoff.ToString());
                command.Parameters.AddWithValue("StoreFront", storeCode);



                using (var da = new SqlDataAdapter(command))
                {
                    conn.Open();

                    da.Fill(ds);
                }

            }
             
            Trace(ds.Tables[0].Rows.Count.ToString() + " records found!\n");

            int counter = 0;
            //Go through each user 
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                var userId = new Guid(row[0].ToString());
                var email = row[1].ToString();

                Trace("-------------------------------------------------------------------");
                Trace("Row: " + (++counter).ToString());

                OrderGroupCollection orders = null;

                // MapUserOrderHistoryToMagentoXML(new Guid(row[0].ToString()), row[1].ToString(), cutoff);
                Trace("Fetching Orders for: " + userId + " email: " + email);

                try
                {
                    orders = _context.GetPurchaseOrdersForUser(userId);
                }
                catch (Exception exception)
                {
                    Trace("FETCH ERROR: " + exception.Message);
                }

                if (orders == null || orders.Count == 0)
                {
                    Trace("No orders found for user!");
                    return;
                }

                Trace(orders.Count.ToString() + " orders found for user!");
                int count = 0;

                XDocument doc = new XDocument(new XDeclaration("1.0", "utf-8", "yes"), new XElement("orders"));

                //get orders for user
                foreach (OrderGroup order in orders)
                {
                    Trace("Order #" + (++count).ToString() + " - " + order.TrackingNumber);

                    string xml = order.GetXml();

                    XDocument input;

                    try
                    {
                        input = XDocument.Parse(xml);
                    }
                    catch (Exception)
                    {
                        Trace("Bad XML!");
                        continue;
                    }


                    if (DateTime.Parse(input.Element("PurchaseOrder").Attribute("Created").Value) < cutoffDate)
                    {
                        Trace("Order is older than cutoff!");
                        continue;
                    }

                    input.Element("PurchaseOrder").SetAttributeValue("UOEmail", email);

                    Trace("Mapping");
                    //map CS XML to Magento XML
                    XElement output = MapCSXMLtoMagentoXML(input, "PurchaseOrder");

                    doc.Element("orders").Add(output);
                }

                if (doc.Element("orders").Elements("order").Count() > 0)
                {
                    //save xml to file
                    Trace("Saving File");
                    doc.Save("MagentoOrders\\" + userId + ".xml");
                }
                else
                {
                    Trace("No files to save!");
                }

                Trace("Done!");
              
            }
             * */
        }
        private static void ExportAllUserMagentoOrderHistory(string storeCode, DateTime cutoffUTC, int pageSize)
        {

            // Set defaults
            //1000 orders per file
            pageSize = pageSize <= 0 ? 1000 : pageSize;

            //Get 360
            storeCode = string.IsNullOrEmpty(storeCode) ? "360" : storeCode;

            //default 1 month ago
            if (cutoffUTC == null)
            {
                DateTime now = DateTime.Now.AddDays(-30);

                cutoffUTC = new DateTime(now.Year, now.Hour, now.Minute);
            }

            DataSet ds = new DataSet();
            Trace("Fetching orders to export");

            using (var conn = new SqlConnection(ConfigurationManager.AppSettings["Transactions"]))
            {
                var command = new SqlCommand();
                command.CommandText = "dbo.getUserMaxOrderDateInfo";
                command.CommandType = System.Data.CommandType.StoredProcedure;
                command.Connection = conn;
                string cutoffDate = cutoffUTC.ToString("yyyy-MM-dd HH:mm:00");
                command.Parameters.AddWithValue("CutoffDate", cutoffDate);
                command.Parameters.AddWithValue("StoreFront", storeCode);


                using (var da = new SqlDataAdapter(command))
                {
                    conn.Open();

                    da.Fill(ds);
                }

            }
            Trace(ds.Tables[0].Rows.Count.ToString() + " records found with Cutoff: " + cutoffUTC.ToString() + " UTC\n");

            //No rows, why bother
            if (ds.Tables[0].Rows.Count == 0)
                return;

            int counter = 0;

            List<XElement> currentList = new List<XElement>();
            List<XElement> orderForUser = new List<XElement>();
            int currentDocCount = 1;

            //Go through each row and get the user's orders
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                Trace("-------------------------------------------------------------------");
                Trace("Row: " + (++counter).ToString() + " User: " + row[0].ToString());


                try
                {
                    orderForUser = MapUserOrderHistoryToMagentoXML(new Guid(row[0].ToString()), row[1].ToString(), cutoffUTC);
                }
                catch (Exception e) //Error with user Order. Log and continue with next one
                {
                    Trace("Caught Exception for User: " + row[0].ToString());
                    LogExceptionToFile(e, "User " + row[0].ToString() + " Unknown Order Problem");
                    orderForUser = null;
                    continue;
                }
                //No orders for that user go to the next user
                if (!orderForUser.Any())
                {
                    Trace("No orders for User: " + row[0].ToString());
                    continue;
                }

                int userOrderCountLimit = 0;
                int orderForUserCount = orderForUser.Count;
                while (userOrderCountLimit < orderForUserCount)
                {
                    //How many more do we need to finish the list
                    int additionalOrdersNeeded = pageSize - currentList.Count;

                    currentList.AddRange(orderForUser.Skip(userOrderCountLimit).Take(additionalOrdersNeeded));
                    //Create doc if needed
                    if (currentList.Count == pageSize)
                    {
                        CreateOrderDoc(currentList, _magentoOrdersPath, cutoffUTC, storeCode, currentDocCount++);
                        //Start a new list
                        currentList.Clear();
                    }
                    userOrderCountLimit += additionalOrdersNeeded;
                }

            }
            //This may be the last doc
            if (currentList.Any())
                CreateOrderDoc(currentList, _magentoOrdersPath, cutoffUTC, storeCode, currentDocCount);
            else
                currentDocCount--;

            Trace("Total Number of Orders: " + _totalOrders + " stored in " + currentDocCount + " files");

        }

        private static void CreateOrderDoc(List<XElement> listOfOrders, string path, DateTime cutoff, string store, int currentDocCount)
        {
            //Create the document
            XDocument doc = new XDocument(new XDeclaration("1.0", "utf-8", "yes"), new XElement("orders"));
            foreach (var order in listOfOrders)
                doc.Element("orders").Add(order);
            string FinalPath = path + "\\" + "MagentoOrder_CO_" + cutoff.ToString("yyyy-MM-dd_") + store + "_" + _deployVersion + "_" + _uniqueFileId + "_" + currentDocCount + ".xml";
            Trace("Saving Order File #" + currentDocCount + " with " + listOfOrders.Count + " orders\n" + FinalPath);
            doc.Save(FinalPath);
        }

        private static void MapAutoShipToMagentoXML(Guid userId, Guid orderId)
        {
            Trace("AutoShip Mapper Engaged!");
            Trace("Initailizing Commerce Server");

            OrderContext context = OrderContext.Create("Data Source=PFDSANDBOXSQL01;Initial Catalog=CSharpSite_transactions;User Id=pfdstoreruntimeuser;Password=fxrk6Bzg;", "Data Source=PFDSANDBOXSQL01;Initial Catalog=CSharpSite_transactionconfig;User Id=pfdstoreruntimeuser;Password=fxrk6Bzg;");

            Basket basket = null;

            try
            {
                basket = context.GetBasket(userId, orderId);
            }
            catch (Exception exception)
            {
                Trace("GET BASKET FAILED!!! Error: " + exception.Message);
            }

            if (basket == null)
            {
                Trace("No basket found for user!");
                return;
            }

            string xml = basket.GetXml();

            XDocument input = null;

            try
            {
                input = XDocument.Parse(xml);
            }
            catch (Exception)
            {
                Trace("Bad XML\n");
                return;
            }

            Trace("Mapping...");
            input.Save("MagentoAutoShip\\" + basket.Name + ".raw.xml");
            //map CS XML to Magento XML
            //XDocument output = MapCSXMLtoMagentoXML(input, "Basket");

            //save xml to file
            Trace("Saving!\n");
            //input.Save("MagentoAutoShip\\" + basket.Name + ".raw.xml");
            //output.Save("MagentoAutoShip\\" + basket.Name + ".mgto.xml");
        }

        private static List<XElement> MapUserOrderHistoryToMagentoXML(Guid userId, string email, DateTime cutoffDateUTC)
        {

            OrderGroupCollection orders = null;
            List<XElement> output = new List<XElement>();

            Trace("Fetching Orders for: " + userId);

            try
            {
                orders = _context.GetPurchaseOrdersForUser(userId);
            }
            catch (Exception exception)
            {
                Trace("FETCH ERROR: " + exception.Message);
            }

            if (orders == null || orders.Count == 0)
            {
                Trace("No orders found for user!");
                return output;
            }

            Trace(orders.Count.ToString() + " orders found for user!");
            int count = 0;

            //get orders for user

            foreach (OrderGroup order in orders)
            {
                Trace("Order #" + (++count).ToString() + " - " + order.TrackingNumber);

                string xml = order.GetXml();

                XDocument input;

                try
                {
                    input = XDocument.Parse(xml);
                }
                catch (Exception)
                {
                    Trace("Bad XML!");
                    continue;
                }

                //Convert back to local time since CommerceServer has date in local time
                DateTime localTime = cutoffDateUTC.ToLocalTime();
                if (DateTime.Parse(input.Element("PurchaseOrder").Attribute("Created").Value) < localTime)
                {
                    Trace("Order is older than cutoff!");
                    continue;
                }

                input.Element("PurchaseOrder").SetAttributeValue("UOEmail", email);

                Trace("Mapping");
                //map CS XML to Magento XML
                try
                {
                    output.Add(MapCSXMLtoMagentoXML(input, "PurchaseOrder"));
                }
                catch (Exception e)
                {
                    LogExceptionToFile(e, "User " + userId + " " + " : Order " + order.TrackingNumber.ToString() + " Map Error");
                    continue; //go to next order
                }

            }

            Trace("User " + userId + " had " + output.Count() + " orders");
            _totalOrders += output.Count;
            return output;
        }

        private static XElement MapCSXMLtoMagentoXML(XDocument input, string type)
        {
            //XDocument input = XDocument.Load(@"c:\projects\magentoxmlmapping\input.xml");
            Trace("Mapping " + type);
            XElement po = input.Element(type);
            XElement of = po.Element("OrderForms").Element("OrderForm");
            XElement cc = of.Element("Payments").Element("CreditCardPayment");
            if (cc == null) //Check GiftCardPayments
            {
                cc = of.Element("Payments").Element("GiftCertificatePayment");
            }
            XElement li = of.Element("LineItems");
            XElement ba = po.Element("Addresses").Elements("OrderAddress").First(a => a.Attribute("OrderAddressId").Value == cc.Attribute("BillingAddressId").Value);
            XElement sa = po.Element("Addresses").Elements("OrderAddress").First(a => a.Attribute("OrderAddressId").Value == of.Element("Shipments").Element("Shipment").Attribute("ShippingAddressId").Value);

            /*
            MD5 md5 = MD5CryptoServiceProvider.Create();
            byte[] hashBytes = md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(email));
            string hash = Convert.ToBase64String(hashBytes);
            */

            float total = float.Parse(po.Attribute("Total").Value);
            float subTotal = float.Parse(po.Attribute("SubTotal").Value);
            float shippingTotal = float.Parse(po.Attribute("ShippingTotal").Value);
            float taxTotal = float.Parse(po.Attribute("TaxTotal").Value);

            string orderTotal = po.Attribute("Total").Value;
            string orderTaxTotal = po.Attribute("TaxTotal").Value;
            string orderTaxPercent = ((taxTotal / (total - taxTotal)) * 100.0f).ToString();
            string orderSubTotal = po.Attribute("SubTotal").Value;
            string orderShippingTotal = po.Attribute("ShippingTotal").Value;
            string orderShippingIncludeTaxTotal = (shippingTotal + taxTotal).ToString();
            string orderSubTotalIncludeTaxTotal = (subTotal + taxTotal).ToString();

            XAttribute trackingNumber = po.Attribute("TrackingNumber");
            string orderTrackingNumber = trackingNumber == null ? String.Empty : trackingNumber.Value;

            string email = po.Attribute("UOEmail").Value.Replace("360_", "").Replace("PFD_", ""); //strip brand prefix?

            //Get Created date to detect old ordes that are InProcess
            DateTime createdTime = DateTime.Parse(po.Attribute("Created").Value);

            //Set to new order state and pending status as default, canceled if canceled
            string orderState =  "new";
            string orderStatus = "pending";


            switch (po.Attribute("Status").Value)
            {
                case "Cancelled":  
                    {
                        orderState = "canceled";
                        orderStatus = "canceled";
                    }
                    break;
                case "InProcess":
                    {
                        if (createdTime < _aMonthAgo)
                        {
                            orderState = "closed";
                            orderStatus = "closed";
                        }
                        else
                        {
                            orderState = "processing";
                            orderStatus = "processing";
                        }
                        break;
                    }
                case "Submitted":
                    {
                        orderState = "closed";
                        orderStatus = "closed";
                    }
                    break;
                case "Shipped":
                    {
                        orderState = "complete";
                        orderStatus = "complete";
                    }
                    break;
                case "NewOrder":
                default:
                    {
                        orderState = "new";
                        orderStatus = "pending";
                    }
                    break;
            }


            Trace("Initialized...");

            XElement output = new XElement("order",
                new XElement("fields",
                    new XElement("increment_id", orderTrackingNumber),  //use TrackingNumber?
                    new XElement("base_currency_code", new XCData("USD")), //hardcoded
                    new XElement("base_grand_total", orderTotal),
                    new XElement("base_tax_amount", orderTaxTotal),
                    new XElement("base_shipping_amount", orderShippingTotal),
                    new XElement("base_shipping_incl_tax", orderShippingIncludeTaxTotal), //INCLUDE TAX?
                    new XElement("base_subtotal", orderSubTotal),
                    new XElement("base_subtotal_incl_tax", orderSubTotalIncludeTaxTotal), //INCLUDE TAX?
                    new XElement("base_total_due", orderTotal),
                    new XElement("billing_address_id", "1"), //hardcoded
                    new XElement("created_at", new XCData(po.Attribute("Created").Value)), //Proper date format? Remove T time separator? UTC?
                    new XElement("customer_id", "1"), //hardcoded 
                    new XElement("customer_email", new XCData(email)),
                    new XElement("customer_firstname", new XCData(ba.Attribute("FirstName").Value)), //taken from billing address
                    new XElement("customer_lastname", new XCData(ba.Attribute("LastName").Value)), //taken from billing address
                    new XElement("customer_middlename", new XCData("")), //hardcoded
                    new XElement("customer_is_guest", "0"),	//hardcoded
                    new XElement("global_currency_code", new XCData("USD")), //hardcoded
                    new XElement("grand_total", po.Attribute("Total").Value),
                    new XElement("order_currency_code", new XCData("USD")), //hardcoded
                    new XElement("shipping_address_id", "2"),
                    new XElement("shipping_amount", orderShippingTotal),
                    new XElement("shipping_incl_tax", orderShippingIncludeTaxTotal),
                    new XElement("shipping_method", new XCData("flatrate_flatrate")), //hardcoded  - We can provide GROUND vs 2DAY vs 1DAY?
                    new XElement("state", new XCData(orderState)), //hardcoded
                    new XElement("status", new XCData(orderStatus)), //hardcoded
                    new XElement("store_currency_code", new XCData("USD")), //hardcoded
                    new XElement("tax_amount", orderTaxTotal),
                    new XElement("tax_percent", orderTaxPercent),
                    new XElement("subtotal", orderSubTotal),
                    new XElement("subtotal_incl_tax", orderSubTotalIncludeTaxTotal),
                    new XElement("total_due", orderTotal),
                    new XElement("total_item_count", po.Attribute("LineItemCount").Value),
                    new XElement("total_qty_ordered", li.Elements("LineItem").Sum(i => (float)i.Attribute("Quantity")))
                    ),
                new XElement("items"),
                new XElement("addresses",
                    new XElement("address",
                        new XElement("entity_id", "1"),
                        new XElement("postcode", new XCData(ba.Attribute("PostalCode").Value)),
                        new XElement("firstname", new XCData(ba.Attribute("FirstName").Value)), //taken from billing address
                        new XElement("lastname", new XCData(ba.Attribute("LastName").Value)), //taken from billing address
                        new XElement("street", new XCData(ba.Attribute("Line1").Value)),
                        new XElement("city", new XCData(ba.Attribute("City").Value)),
                        new XElement("region", new XCData(ba.Attribute("State").Value)),
                        new XElement("email", new XCData(email)), //strip brand prefix?  copied from order header?
                        new XElement("telephone", new XCData(ba.Attribute("DaytimePhoneNumber").Value)),
                        new XElement("country_id", new XCData("US")), //hardcoded
                        new XElement("address_type", new XCData("billing")) //hardcoded
                    ),
                    new XElement("address",
                        new XElement("entity_id", "2"),
                        new XElement("postcode", new XCData(sa.Attribute("PostalCode").Value)),
                        new XElement("firstname", new XCData(sa.Attribute("FirstName").Value)), //taken from billing address
                        new XElement("lastname", new XCData(sa.Attribute("LastName").Value)), //taken from billing address
                        new XElement("street", new XCData(sa.Attribute("Line1").Value)),
                        new XElement("city", new XCData(sa.Attribute("City").Value)),
                        new XElement("region", new XCData(sa.Attribute("State").Value)),
                        new XElement("email", new XCData(email)), //strip brand prefix?  copied from order header?
                        new XElement("telephone", new XCData(sa.Attribute("DaytimePhoneNumber").Value)),
                        new XElement("country_id", new XCData("US")), //hardcoded
                        new XElement("address_type", new XCData("shipping")) //hardcoded
                    )
                ),
                new XElement("payments",
                    new XElement("payment",
                    new XElement("base_shipping_amount", orderShippingTotal),
                    new XElement("shipping_amount", orderShippingTotal),
                    new XElement("base_amount_ordered", cc.Attribute("Amount").Value),
                    new XElement("amount_ordered", cc.Attribute("Amount").Value),
                    new XElement("method", new XCData("checkmo")), //can provide Visa, MC, Amex, Disc - what format?
                    new XElement("amount", cc.Attribute("Amount").Value)
                    )
                ),
                new XElement("invoices"),
                new XElement("shipments"),
                new XElement("creditmemos")
                );

            foreach (XElement line in li.Elements("LineItem"))
            {
                XElement item = new XElement("item",
                                    new XElement("product_type", new XCData("simple")), //hardcoded
                                    new XElement("sku", new XCData(line.Attribute("ProductVariantId").Value.Replace("(BaseCatalog)", ""))), //using variant id?  trim "(BaseCatalog)"?
                                    new XElement("name", new XCData(line.Attribute("DisplayName").Value.Replace("'", "&#039;"))),
                                    new XElement("qty_ordered", line.Attribute("Quantity").Value),
                                    new XElement("price", line.Attribute("PlacedPrice").Value),
                                    new XElement("base_price", line.Attribute("PlacedPrice").Value),
                                    new XElement("original_price", line.Attribute("PlacedPrice").Value),
                                    new XElement("base_original_price", line.Attribute("PlacedPrice").Value),
                                    new XElement("row_total", line.Attribute("PlacedPrice").Value)
                    //new XElement("base_row_total", line.Attribute("PlacedPrice").Value),
                    //new XElement("price_incl_tax", line.Attribute("PlacedPrice").Value),  //tax?
                    //new XElement("base_price_incl_tax", line.Attribute("PlacedPrice").Value), //tax?
                    //new XElement("row_total_incl_tax", line.Attribute("PlacedPrice").Value), //tax?
                    //new XElement("base_row_total_incl_tax", line.Attribute("PlacedPrice").Value) //tax?
                                    );

                output.Element("items").Add(item);
            }

            return output;
        }

        static private void ExtractMarshalledData()
        {
            Trace("Marshalled Data Extractor Engaged!");
            Trace("Reading Config...");

            int maxBatchSize = 0;
            int lookBackWindowHours = 1;

            int.TryParse(ConfigurationManager.AppSettings["DataExtract_MaxBatchSize"], out maxBatchSize);
            int.TryParse(ConfigurationManager.AppSettings["DataExtract_LookBackWindowHours"], out lookBackWindowHours);

            Trace("Initailizing Commerce Server");
            OrderContext context = OrderContext.Create(ConfigurationManager.AppSettings["Transactions"], ConfigurationManager.AppSettings["TransactionsConfig"]);
            DataSet ds = new DataSet();
            string sql = String.Format("exec GetOrdersForMarshalledDataExtraction {0}, {1}", maxBatchSize, lookBackWindowHours);

            //Trace(ConfigurationManager.AppSettings["Transactions"]);

            Trace(sql);

            Trace("Fetching orders to process");

            using (var conn = new SqlConnection(ConfigurationManager.AppSettings["Transactions"]))
            {
                using (var da = new SqlDataAdapter(sql, conn))
                {
                    conn.Open();

                    da.Fill(ds);
                }
            }

            Trace(ds.Tables[0].Rows.Count.ToString() + " records found!\n");

            Trace("Processing orders");

            bool simulateOnly = false;
            bool.TryParse(ConfigurationManager.AppSettings["DataExtract_SimulateOnly"], out simulateOnly);
            List<string> westCoastStates = ConfigurationManager.AppSettings["DataExtract_WestCoastStates"].Split(',').ToList();
            //List<string> taxedStates = "PA".Split(',').ToList(); //,CO,NV,KY,CA
            CatalogEntities catalogContext = new CatalogEntities(ConfigurationManager.AppSettings["CatalogEntities"]);
            var rcvdItems = catalogContext.BaseCatalog_CatalogProducts.Where(i => (i.DropShip == true && i.name.ToLower().Contains("royal canin"))).Select(i => i.VariantID).ToList();
            List<string> holdList = ConfigurationManager.AppSettings["DataExtract_HoldList"].Split(',').ToList();

            using (var conn = new SqlConnection(ConfigurationManager.AppSettings["Transactions"]))
            {
                conn.Open();

                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    string userId = dr["SoldToId"].ToString();
                    string orderId = dr["OrderGroupId"].ToString();

                    Trace("-------------------------------------------------------------------");
                    Trace(String.Format("User:{0} \\ Order:{1}", userId, orderId));

                    PurchaseOrder order;

                    try
                    {
                        order = context.GetPurchaseOrder(new Guid(userId), new Guid(orderId));
                    }
                    catch (Exception exception)
                    {
                        Trace("FETCH ERROR (see log): " + exception.Message);
                        LogExceptionToFile(exception, orderId);
                        continue;
                    }

                    if (order == null)
                    {
                        Trace("FETCH ERROR: Order Not Found (NULL)");
                        continue;
                    }

                    string xml = order.GetXml();

                    XDocument doc;

                    try
                    {
                        doc = XDocument.Parse(xml);

                        //Trace("Removing Non-ASCII characters from product descriptions");
                        //fix bad unicode data in product descriptions
                        var lineItems = doc.Descendants("LineItem");

                        foreach (XElement lineItem in lineItems)
                        {
                            lineItem.SetAttributeValue("DisplayName", StripNonASCIICharacters(lineItem.Attribute("DisplayName").Value));
                            //Trace("\t" + lineItem.Attribute("DisplayName").Value);
                        }

                        //extract data
                        var store = doc.Descendants("WeaklyTypedProperty").First(p => p.Attribute("Name").Value == "SiteId").Attribute("Value").Value.Substring(0, 3);
                        var orderName = doc.Element("PurchaseOrder").Attribute("Name").Value.ToUpper();
                        var shippingAddressId = doc.Element("PurchaseOrder").Element("OrderForms").Element("OrderForm").Element("Shipments").Element("Shipment").Attribute("ShippingAddressId").Value;
                        var shippingState = doc.Descendants("OrderAddress").First(a => a.Attribute("OrderAddressId").Value == shippingAddressId).Attribute("State").Value;

                        Dictionary<string, bool> dataExtracts = new Dictionary<string, bool>();
                        dataExtracts.Add(store + ".All", true);
                        dataExtracts.Add(store + ".IsAutoShip", orderName.StartsWith("A"));
                        dataExtracts.Add(store + ".HasRXItem", doc.Descendants("WeaklyTypedProperty").Any(l => l.Attribute("Name").Value == "isRXItem" && l.Attribute("Value").Value == "true"));
                        dataExtracts.Add(store + ".HasFreeShippingItem", doc.Descendants("WeaklyTypedProperty").Any(l => l.Attribute("Name").Value == "isFreeShipping" && l.Attribute("Value").Value == "true"));
                        dataExtracts.Add(store + ".HasHIN", doc.Descendants("WeaklyTypedProperty").Any(l => l.Attribute("Name").Value == "VetClinicHIN" && l.Attribute("Value").Value.Length > 0));
                        dataExtracts.Add(store + ".HasPet", doc.Descendants("WeaklyTypedProperty").Any(l => l.Attribute("Name").Value == "petId" && l.Attribute("Value").Value.Length > 0));
                        dataExtracts.Add(store + ".HasVet", doc.Descendants("WeaklyTypedProperty").Any(l => l.Attribute("Name").Value == "vetId" && l.Attribute("Value").Value.Length > 0));
                        dataExtracts.Add(store + ".HasRCVDItem", doc.Descendants("LineItem").Any(l => rcvdItems.Contains(l.Attribute("ProductVariantId").Value.Replace("(BaseCatalog)", ""))));
                        dataExtracts.Add(store + ".IsChino", westCoastStates.Contains(shippingState));
                        //dataExtracts.Add(store + ".IsTaxed", taxedStates.Contains(shippingState));
                        dataExtracts.Add(store + ".IsPA", shippingState == "PA");

                        var positiveExtracts = dataExtracts.Where(e => e.Value == true).Select(e => e.Key);

                        foreach (var extract in positiveExtracts)
                        {
                            SendMetricToGraphite("commerceserver.orders." + extract, 1);
                        }

                        string matches = String.Join(",", positiveExtracts.ToArray());

                        Trace("Extracts: " + matches);

                        bool hold = holdList.Any(h => dataExtracts.ContainsKey(h) && dataExtracts[h] == true);

                        if (hold)
                        {
                            Trace("Placed on HOLD!");
                        }

                        sql = "insert into PurchaseOrdersMarshalledData ("
                            + "OrderGroupId, "
                            + "MarshalledData, "
                            + "Store, "
                            + "IsAutoShip, "
                            + "IsChino, "
                            + "HasRXItem, "
                            + "HasFreeShippingItem, "
                            + "HasHIN, "
                            + "HasPet, "
                            + "HasVet, "
                            + "HasRCVDItem, "
                            + "Hold) "
                            + "values ('{0}', '{1}', '{2}', {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11})";


                        sql = String.Format(sql,
                            orderId,
                            doc.ToString().Replace("'", "''"),
                            store,
                            Convert.ToInt16(dataExtracts[store + ".IsAutoShip"]),
                            Convert.ToInt16(dataExtracts[store + ".IsChino"]),
                            Convert.ToInt16(dataExtracts[store + ".HasRXItem"]),
                            Convert.ToInt16(dataExtracts[store + ".HasFreeShippingItem"]),
                            Convert.ToInt16(dataExtracts[store + ".HasHIN"]),
                            Convert.ToInt16(dataExtracts[store + ".HasPet"]),
                            Convert.ToInt16(dataExtracts[store + ".HasVet"]),
                            Convert.ToInt16(dataExtracts[store + ".HasRCVDItem"]),
                            Convert.ToInt16(hold));
                    }
                    catch (Exception exception)
                    {
                        Trace("EXTRACTION ERROR (see log): " + exception.Message);

                        string errorMessage = exception.Message + "\r\n\r\n" + exception.StackTrace;
                        sql = String.Format("insert into PurchaseOrdersMarshalledData (OrderGroupId, Error, Hold) values ('{0}', '{1}', 1)", orderId, errorMessage);

                        LogExceptionToFile(exception, orderId);
                    }

                    if (simulateOnly)
                    {
                        Trace("SimulateOnly=TRUE (Skipping INSERT)");
                        continue;
                    }

                    //update POMD table
                    using (var cmd = new SqlCommand(sql, conn))
                    {
                        try
                        {
                            int rows = cmd.ExecuteNonQuery();
                        }
                        catch (Exception exception)
                        {
                            Trace("INSERT ERROR (see log):" + exception.Message);

                            ApplicationException customException = new ApplicationException(exception.Message + " - SQL Statement: \r\n\r\n" + sql, exception);
                            LogExceptionToFile(customException, orderId);
                            continue;
                        }
                    }

                    Trace("Complete!");
                }
            }
        }

        private static void SendMetricToGraphite(string metricName, int metricValue)
        {
            if (_ipEndPoint == null)
            {
                return;
            }

            try
            {
                //string message = String.Format("{0}.{1} {2}\n", _graphiteAPIKey, metricName, metricValue);
                string message = String.Format("{0}.{1}:{2}|c", _graphiteAPIKey, metricName, metricValue);
                var bytes = Encoding.ASCII.GetBytes(message);
                var sock = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp) { Blocking = false };
                sock.SendTo(bytes, _ipEndPoint);
            }
            catch (Exception exception)
            {
                //we don't want errors with metrics interfering with the job so just log it
                Trace("GRAPHITE ERROR (see log): " + exception.Message);
                LogExceptionToFile(exception, "GRAPHITE");
            }
        }

        private static string StripNonASCIICharacters(string input)
        {
            byte[] asciiChars = Encoding.ASCII.GetBytes(input);
            return Encoding.ASCII.GetString(asciiChars);
        }

        private static void Trace(string message)
        {
            Console.WriteLine(message);

            if (_traceToLog && _traceLogWriter != null)
            {
                string messageFormat = String.Format("{0} - {1}", DateTime.Now, message);
                _traceLogWriter.WriteLine(messageFormat);
                _traceLogWriter.Flush();
            }

        }

        private static void LogExceptionToFile(Exception exception, string orderId)
        { 
            _errorLogWriter.WriteLine(String.Format("{0} - Order/Reason: {1} - {2}\r\n\r\n{3}\r\n-----------\r\n", DateTime.Now, orderId, exception.Message, exception.StackTrace));
            _errorLogWriter.Flush();
        }
    }
}
